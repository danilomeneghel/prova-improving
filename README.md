# Test Improving

## Octo Events

Octo Events is an application that listens to Github Events via webhooks and expose by an api for later use.

![alt text](imgs/octo_events.png)

 The test consists in building 2 endpoints:

## 1. Webhook Endpoint

The Webhook endpoint receives events from Github and saves them on the database, in order to do that you must read the following docs:

* Webhooks Overview: https://developer.github.com/webhooks/ 
* Creating Webhooks : https://developer.github.com/webhooks/creating/

It must be called `/events`

## 2. Events Endpoint

The Events endpoint will expose the persist the events by an api that will filter by issue number

**Request:**

> GET /issues/1000/events

**Response:**

> 200 OK
```javascript
[ 
  { "action": "open", created_at: "...",}, 
  { "action": "closed", created_at: "...",} 
]
```

**Github Integration Instructions**

* Tip: You can use ngrok (https://ngrok.com/)  to install / debug the webhook calls, it generates a public url that will route to your local host:

   $ sudo ngrok http 4000 

![alt text](imgs/ngrok.png)

   GitHub

![alt text](imgs/add_webhook.png)
 
**Final Observations**

* Use any library / framework / gem  you want, you don't have to do anything "from scratch"
* Write tests, use your favorite framework for that
* Use Postgres 9.6+ or MySQL 5.7+ for your database;
* Add to README.md your instructions for running the project. Whether you're delivering just source code or an accompanying `Dockerfile`/`docker-compose.yml`, we expect at most the following commands to be needed to run your solution (besides the usual docker related deploy steps):
    - `rake db:create`
    - `rake db:migrate`
    - `rails s -p 3000 -b '0.0.0.0'`
* The oldest supported Ruby version is 2.5.1;
* Have fun and we hope you succeed :-)

--------------------------------------------------------

## Features

- API REST
- Routes
- Migrate
- Seed

## Requirements

- Ruby >= 2.5.8
- Rails >= 5.1.7
- Rake >= 13.0.1
- PostgreSQL >= 12

## Tecnologies

- Ruby
- Rails
- PostgreSQL

## Installation

```
$ git clone https://danilomeneghel@bitbucket.org/recrutamento_jya_ruby/recrutamento-ruby-jya-danilo.meneghel_gmail.com.git

$ cd recrutamento-ruby-jya-danilo.meneghel_gmail.com
$ gem install bundler && bundle install

$ rake db:create
$ rake db:migrate
$ rake db:seed
```

After everything is done, run the project:

```
$ rails s -p 3000 -b '0.0.0.0'
```

## Docker 

To run the project using Docker, execute the following commands on the terminal:

```
$ docker-compose build
$ docker-compose up

$ docker-compose run web rake db:create
$ docker-compose run web rake db:migrate
$ docker-compose run web rake db:seed
```

Finally open in your browser:

http://localhost:3000/api/v1/events

## Routes

http://localhost:3000/rails/info/routes

## Demonstration

You can see the system working by clicking the link below:

https://prova-improving.herokuapp.com/api/v1/events

## License

This project is licensed under <a href="LICENSE">The MIT License (MIT)</a>.

## Screenshots

![Screenshots](screenshots/screenshot01.png)

![Screenshots](screenshots/screenshot02.png)

![Screenshots](screenshots/screenshot03.png)

![Screenshots](screenshots/screenshot04.png)




Developed by

Danilo Meneghel

danilo.meneghel@gmail.com

http://danilomeneghel.github.io/
