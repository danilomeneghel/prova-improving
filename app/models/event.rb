class Event < ApplicationRecord
	validates :action, presence: true
	validates :number, presence: true
end
